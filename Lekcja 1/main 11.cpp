#include <iostream>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	int n;
	int wynik;
	
	std::cout<<"Podaj rozmiar macierzy:\n";
	std::cin>>n;
	std::cout<<std::endl;
	
	for (int i=1; i<=n;i++){
		for(int j=1; j<=n ; j++){
			wynik = i*j ;
			std::cout<<""<<i<<"*"<<j<<"="<<wynik;
			std::cout<<std::endl;
		}
		std::cout<<std::endl;
	}
	return 0;
}
